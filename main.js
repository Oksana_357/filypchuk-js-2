const { City } = require('./City')
const { Capital } = require('./Capital')
const { Country } = require('./Country')

let city1 = new City('Kyiv')
let city2 = new City('Lviv')
let city3 = new City('Kharkiv')
let city4 = new City('Poltava')
let city5 = new City('Dnipro')
let city6 = new City('Odesa')
let city7 = new City('Chernivtsi')
let city8 = new City('Khmelnytsky')
let city9 = new City('Sumy')
let city10 = new City('Uzhhorod')

city1.setWeather().then(() => { console.log(city1) }).catch((error) => { console.log(error) })
city2.setWeather().then(() => { console.log(city2) }).catch((error) => { console.log(error) })

let capital = new Capital('Kyiv')
capital.setAirport('Boryspil')
capital.setWeather().then(() => { console.log(capital) }).catch((error) => { console.log(error) })

let country = new Country('Ukraine')
country.addCity(city1)
country.addCity(city2)
country.addCity(city3)
country.addCity(city4)
country.addCity(city5)
country.addCity(city6)
country.addCity(city7)
country.addCity(city8)
country.addCity(city9)
country.addCity(city10)

function sortByTemperature(a, b) {
    let aInt = parse(a.weather.temperature)
    let bInt = parse(b.weather.temperature)
    if (aInt > bInt)
        return -1
    if (aInt < bInt)
        return 1
    return 0
}

async function setWeathers() {
    const promises = country.cities.map((v) => v.setWeather())
    await Promise.all(promises)
    return country.cities
};

function parse(x) {
    const parsed = parseInt(x, 10)
    if (isNaN(parsed)) { return 0 }
    return parsed
}

setWeathers().then((cities) => {
    cities.sort(sortByTemperature)
    cities.forEach(city => console.log(city))
})