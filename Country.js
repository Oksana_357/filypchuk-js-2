const City = require('./City')

class Country {

    constructor(name) {
        this.cities = []
        this.name = name
    }

    addCity(city) {
        this.cities.push(city)
    }

    removeCity(name) {
        let index = this.cities.findIndex(city => city.name === name)
        if (index != -1) {
            this.cities.splice(index, 1)
        }
    }
}

module.exports = { Country }

